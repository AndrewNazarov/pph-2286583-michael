# Custom shape picture editor for use in a web page (hexagon shape)

## Dependencies

1. Canvas library "[Konva.js](https://konvajs.org/)" 
2. AWS S3 Bucket script ([tutorial](https://docs.aws.amazon.com/en_us/sdk-for-javascript/v2/developer-guide/s3-example-photo-album.html))

## How to use

You need simple HTML

    <div id="container"></div>
    
    <input type="file" id="imageLoader" name="imageLoader" accept="image/*">
    
    <button id="saveImage">Save as image</button>

application script

    <script src="js/app.js"></script>

and initialize constructor

    new konvaInit();

#### Also you can use a constructor with parameters

    new KonvaInit({
                containerId: 'container', // ID of the tag for the canvas insertion (default: container)
                width: 222, // canvas width (default: 400)
                height: 204, // canvas height (default: 400)
                placeholderUrl: 'img/placeholder.jpg', // path to the placeholder picture (default: placeholder.jpg)
                fileLoadInputId: 'imageLoader', // ID of the tag <input type="file"> for load the image (default: imageLoader)
                resetButtonId: 'resetImage', // ID of the tag (button) for reset the image (default: resetImage)
                saveImageButtonId: 'saveImage', // ID of the tag (button) for save the image (default: saveImage)
                rangeInputId: 'range', // ID of the tag <input type="range"> for change the picture scale via the slider (default: range)
                s3: {
                    // S3 Bucket params (https://docs.aws.amazon.com/en_us/sdk-for-javascript/v2/developer-guide/s3-example-photo-album.html)
                    albumBucketName: 'BUCKET_NAME',
                    bucketRegion: 'REGION',
                    IdentityPoolId: 'IDENTITY_POOL_ID',
                    imageNameToSave: 'UserId' // image file name to save to S3 Bucket (unique! don't use the timestamp as part of the name)
                }
            });


#### Methods you can modify in the "app.js" to

Change the shape:

    this.shapeFunc(canvasContext)

Set image save location ( local machine / AWS S3 Bucket )

    this.initSaveFunc(konvaStageLink)
    
Set the callback function after image saving

    this.imageLoadedSuccessCallback()


Look [the results of the editor use](https://www.youtube.com/watch?v=xIKoMMPwFko).