
function KonvaInit(attrs) {
    var self = this;
    attrs = attrs || {};

    this.containerId =  attrs.containerId   || 'container';
    this.width =        attrs.width         || 400;
    this.height =       attrs.height        || 400;
    this.placeholderUrl =  attrs.placeholderUrl || 'img/placeholder.jpg';
    this.fileLoadInputId = attrs.fileLoadInputId || 'imageLoader';
    this.resetButtonId = attrs.resetButtonId || 'resetImage';
    this.saveImageButtonId = attrs.saveImageButtonId || 'saveImage';
    this.rangeInputId =  attrs.rangeInputId || 'range';
    this.s3 = {
        storage: {},
        accessParams: {}
    };

    if (attrs.s3) {
        this.s3.accessParams = {
            albumBucketName: attrs.s3.albumBucketName || 'BUCKET_NAME',
            bucketRegion: attrs.s3.bucketRegion || 'REGION',
            IdentityPoolId: attrs.s3.albumBucketName || 'IDENTITY_POOL_ID',
            imageNameToSave: attrs.s3.imageNameToSave || 'UserId'
        }
    }

    this.shapeParams = {
        minX: 0,
        minY: 0,
        maxX: 0,
        maxY: 0,
        height: 0,
        width: 0
    };
    this.imageParams = {
        baseSize: {
            width: 0,
            height: 0
        },
        minSize: {
            width: 0,
            height: 0
        },
        maxSize: {
            width: 0,
            height: 0
        },
        currentSize: {
            width: 0,
            height: 0
        },
        currentPos: {
            x: 0,
            y: 0
        },
        ratio: {
            kW: 1,
            kH: 1
        },
        scaleParams: {
            min: 0,
            max: 1,
            step: 0.1
        },
        scale: 1 // range form 0 to 1
    };

    this.obj = {}; // for an access to main objects (stage, layer, image)
    this.domElements = {};

    this.shapeFunc = function (ctx) {
        var shapeArr = [
            [0, 123.9],[58.4, 202.8],[157.6, 202.8],[220, 79],[161.6, 0],[62.4, 0]
        ];

        var
            lineToX,
            lineToY,
            tempArrX = [],
            tempArrY = [];

        ctx.beginPath();

        var
            moveToX = shapeArr[0][0],
            moveToY = shapeArr[0][1];

        ctx.moveTo(moveToX, moveToY);
        tempArrX.push(moveToX);
        tempArrY.push(moveToY);

        for (var i = 1; i < shapeArr.length; i++) {
            lineToX = shapeArr[i][0];
            lineToY = shapeArr[i][1];

            ctx.lineTo(lineToX, lineToY);
            tempArrX.push(lineToX);
            tempArrY.push(lineToY);
        }

        ctx.closePath();

        // shape bounds
        this.shapeParams.minX = Math.min.apply(null, tempArrX);
        this.shapeParams.minY = Math.min.apply(null, tempArrY);
        this.shapeParams.maxX = Math.max.apply(null, tempArrX);
        this.shapeParams.maxY = Math.max.apply(null, tempArrY);
        this.shapeParams.height = this.shapeParams.maxY - this.shapeParams.minY;
        this.shapeParams.width = this.shapeParams.maxX - this.shapeParams.minX;

        return ctx;
    };

    this.createMainGroup = function () {
        var mainGroup = new Konva.Group();

        return mainGroup;
    };

    this.createGroupImage = function () {
        var groupImage = new Konva.Group({
            clipFunc: function(ctx) {
                self.shapeFunc(ctx);
            },
            draggable: true
        });

        groupImage.on('mouseover', function() {
            document.body.style.cursor = 'move';
        });
        groupImage.on('mouseout', function() {
            document.body.style.cursor = 'default';
        });

        return groupImage;
    };

    this.createBoundShape = function () {
        var shape = new Konva.Shape({
            sceneFunc: function (ctx, shape) {
                self.shapeFunc(ctx);

                ctx.closePath();
                ctx.fillStrokeShape(shape);
            },
            stroke: 'black',
            strokeWidth: 1
        });

        return shape;
    };

    this.loadImage = function (imageObj, imageSrc) {
        var
            self = this,
            groupImage = self.obj.groupImage,
            layerImage = self.obj.layerImage,
            stage = self.obj.stage;

        imageObj.onload = function(e){

            // reset the range after the previous picture editing
            self.resetRange();

            var el = e.path[0],

                shapeWidth = self.shapeParams.width,
                shapeHeight = self.shapeParams.height,

                currentWidth = shapeWidth,
                currentHeight = shapeWidth / el.width * el.height,

                kW = shapeWidth / el.width,
                kH = shapeHeight / el.height,
                minSize = {},
                maxSize = {};

            var fileSize = document.getElementById('fileSize');
            if (fileSize) {
                fileSize.innerHTML = el.width + 'x' + el.height;
            }

            self.imageParams.ratio.kW = kW;
            self.imageParams.ratio.kH = kH;

            self.imageParams.baseSize.width = el.width;
            self.imageParams.baseSize.height = el.height;

            if (kW <= kH) {
                currentHeight = shapeHeight;
                currentWidth = el.width * kH;
            }
            else {
                currentWidth = shapeWidth;
                currentHeight = el.height * kW;
            }


            minSize = {
                width: currentWidth,
                height: currentHeight
            };

            maxSize = {
                width: el.width,
                height: el.height
            };



            self.imageParams.minSize = minSize;
            self.imageParams.maxSize = maxSize;

            self.imageParams.currentSize.height = currentHeight;
            self.imageParams.currentSize.width = currentWidth;


            // function for start image size
            var currentImage = new Konva.Image({
                x: (shapeWidth - currentWidth)/2 + self.shapeParams.minX,
                y: (shapeHeight - currentHeight)/2 + self.shapeParams.minY,
                //offset: {
                //    // use offset for  easy rotation (around the center of the image)
                //    x: currentWidth/2,
                //    y: currentHeight/2
                //},
                image: imageObj,
                width: currentWidth,
                height: currentHeight,
                draggable: true,
                dragBoundFunc: self.boundFunc
            });

            self.obj.currentImage = currentImage;

            self.imageParams.currentPos = currentImage.position();

            // remove prevent image
            groupImage.destroyChildren();

            // add a new image
            groupImage.add(self.obj.currentImage);

            // add the shape to the layer
            layerImage.add(groupImage);

            // add the layer to the stage
            stage.add(layerImage);

            // rotation func
            //currentImage.rotation(30);
            //layerImage.draw();
        };
        imageObj.src = imageSrc;
    };

    this.createHandleImage = function () {
        var imageObj = new Image();

        var imageLoader = document.getElementById(self.fileLoadInputId);
        imageLoader.addEventListener('change', handleImage, false);

        self.domElements.imageLoader = imageLoader;

        function handleImage(e){
            var reader = new FileReader();

            reader.onload = function(event){

                self.loadImage(imageObj, event.target.result);

            };
            reader.readAsDataURL(e.target.files[0]);
        }
    };

    this.boundFunc = function (pos) {
        var
            image = self.obj.currentImage,
            shapeParams = self.shapeParams,
            x = pos.x,
            y = pos.y;

        var imageCurrentSize = {
            width: image.width(),
            height: image.height()
        };

        y = Math.max(y, shapeParams.maxY - imageCurrentSize.height); // to top until had stick at the bottom
        y = Math.min(y, shapeParams.minY); // to bottom until had stick at the top

        x = Math.max(x, shapeParams.maxX - imageCurrentSize.width); // to left until had stick at the right
        x = Math.min(x, shapeParams.minX); // to right until had stick at the left

        return {
            x: x,
            y: y
        };
    };

    this.checkScaleRange = function (scale) {
        var
            scaleMin = +self.imageParams.scaleParams.min,
            scaleMax = +self.imageParams.scaleParams.max;

        // set scale bounds
        var newScale = Math.min(scaleMax, Math.max(scale, scaleMin));

        if (self.domElements.range) {
            self.domElements.range.value = newScale;
        }

        return newScale;
    };

    this.scaleChangeFunc = function (scale) {

        // range limits
        self.imageParams.scale = self.checkScaleRange(scale);

        // an image was not load yet
        if (!self.obj.currentImage) return;

        // the picture smaller then the shape (both sides)
        // in this wey user can't change scale
        if (Math.max(self.imageParams.ratio.kW, self.imageParams.ratio.kH) > 1) return;

        var
            minWidth = self.imageParams.minSize.width,
            maxWidth = self.imageParams.maxSize.width,
            minHeight = self.imageParams.minSize.height,
            maxHeight = self.imageParams.maxSize.height,

            startWidth = self.obj.currentImage.width(),
            endWidth,
            startHeight = self.obj.currentImage.height(),
            endHeight;

        // calc new size
        endWidth = minWidth + (maxWidth - minWidth) * self.imageParams.scale;
        endHeight = minHeight + (maxHeight - minHeight) * self.imageParams.scale;

        // set new size
        self.obj.currentImage.width(endWidth);
        self.obj.currentImage.height(endHeight);

        var
            posX = self.obj.currentImage.x() - (endWidth - startWidth)/ 2,
            posY = self.obj.currentImage.y() - (endHeight - startHeight)/2;

        var pos = self.boundFunc({
            x: posX,
            y: posY
        });

        // for test
        //var pos = {
        //    x: posX,
        //    y: posY
        //};

        // set new position
        self.obj.currentImage.x(pos.x);
        self.obj.currentImage.y(pos.y);

        self.obj.layerImage.draw();
    };

    this.initScaleFunc = function () {
        var
            range = document.getElementById(self.rangeInputId),
            scaleStep = +self.imageParams.scaleParams.step;

        self.domElements.range = range;

        // input slider
        if (range) {
            range.setAttribute('min', self.imageParams.scaleParams.min.toString());
            range.setAttribute('max', self.imageParams.scaleParams.max.toString());
            range.setAttribute('step', self.imageParams.scaleParams.step.toString());

            range.addEventListener('input', function(e) {
                var scale = +this.value;

                self.scaleChangeFunc(scale);
            }, false );
        }

        // mouse wheel
        self.obj.stage.on('wheel', function (e) {
            e.evt.preventDefault();

            var
                changeTo = e.evt.deltaY > 0 ? -scaleStep : scaleStep,
                newScale = self.imageParams.scale + changeTo;

            self.scaleChangeFunc(newScale);
        });

        self.resetRange();
    };

    this.resetRange = function () {
        self.scaleChangeFunc(0);
    };

    this.resetImage = function () {
        self.domElements.imageLoader.value = null;

        if (self.placeholderUrl) {
            self.loadImage(new Image(), self.placeholderUrl);
        }
    };

    this.initResetImageFunc = function () {

        // set the placeholderUrl in the first time
        self.resetImage();

        document.getElementById(self.resetButtonId).addEventListener(
            'click',
            function(e) {
                e.preventDefault();

                self.resetImage();
            },
            false
        );
    };

    this.initSaveFunc = function (stage) {

        self.initS3Bucket();

        function downloadURI(uri, name) {
            var link = document.createElement('a');
            link.download = name;
            link.href = uri;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            delete link;
        }

        document.getElementById(self.saveImageButtonId).addEventListener(
            'click',
            function(e) {
                e.preventDefault();

                var image = stage.toDataURL();

                // download image to the local machine
                //downloadURI(image, 'stage.png');

                // download image to the S3 Bucket
                self.saveImageToS3(image);
            },
            false
        );
    };

    this.initS3Bucket = function () {
        var
            s3Params = self.s3.accessParams,

            albumBucketName = s3Params.albumBucketName,
            bucketRegion = s3Params.bucketRegion,
            IdentityPoolId = s3Params.IdentityPoolId;

        AWS.config.update({
            region: bucketRegion,
            credentials: new AWS.CognitoIdentityCredentials({
                IdentityPoolId: IdentityPoolId
            })
        });

        self.s3.storage = new AWS.S3({
            apiVersion: '2006-03-01',
            params: {Bucket: albumBucketName}
        });
    };

    this.saveImageToS3 = function (stage) {
        var
            s3 = self.s3.storage,
            albumName = self.s3.accessParams.albumBucketName,

            file = stage,
            fileName = self.s3.accessParams.imageNameToSave,
            albumPhotosKey = encodeURIComponent(albumName) + '//',

            photoKey = albumPhotosKey + fileName;

        s3.upload({
            Key: photoKey,
            Body: file,
            ACL: 'public-read'
        }, function(err, data) {
            if (err) {
                return console.error('There was an error uploading user photo: ', err.message);
            }

            // callback success
            self.imageLoadedSuccessCallback();
        });
    };

    this.imageLoadedSuccessCallback = function () {
        console.log('User photo was loaded successfully');
    };

    this.init = function () {
        var stage = new Konva.Stage({
            container: this.containerId,
            width: this.width,
            height: this.height,
            x: 0.5,
            y: 0.5
        });

        var layer = new Konva.Layer();
        var layerImage = new Konva.Layer();

        var mainGroup = this.createMainGroup();

        stage.add(layer);

        var shape = this.createBoundShape();
        mainGroup.add(shape);

        var groupImage = this.createGroupImage();
        mainGroup.add(groupImage);

        this.createHandleImage(groupImage, layerImage, stage);

        layer.add(mainGroup);

        layer.draw();

        this.obj.stage = stage;
        this.obj.layer = layer;
        this.obj.layerImage = layerImage;
        this.obj.groupImage = groupImage;

        this.initScaleFunc();

        this.initSaveFunc(stage);

        this.initResetImageFunc();

    };


    this.init();
}